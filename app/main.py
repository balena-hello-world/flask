from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/<name>')
def hello_world_named(name):
    return 'Hello, %s!' % (name.capitalize())
